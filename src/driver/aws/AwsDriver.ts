import { SQSClient, ListQueuesCommand } from "@aws-sdk/client-sqs";
import { SNSClient, ListTopicsCommand, PublishCommand, PublishCommandInput } from "@aws-sdk/client-sns";
import { PublishMessage, Message } from '../../message';
import { Consumer } from 'sqs-consumer';
import { Driver } from '../Driver';
import { Connection } from '../../connection';
import { AwsConnectionOptions } from './AwsConnectionOptions';
import { Subscriber } from '../../subscribers';

const defaultLogger: any = {
    info(message: string) {
        console.log(message);
    },

    error(message: string) {
        console.log(message);
    },

    warn(message: string) {
        console.warn(message);
    }
};

export class AwsDriver implements Driver {
    public subscribers: Subscriber<any, any>[];
    public options: AwsConnectionOptions;
    private client: Consumer;
    private logger: any;
    public sns: SNSClient;
    public sqs: SQSClient;

    constructor(connection: Connection) {
        this.logger = connection.logger || defaultLogger;
        this.options = connection.options as AwsConnectionOptions;
        this.subscribers = connection.subscribers;

        this.sns = new SNSClient();
        this.sqs = new SQSClient();
        try {
            if (this.options.queueUrl) {
                this.client = Consumer.create({
                    queueUrl: this.options.queueUrl,
                    handleMessage: this.handleMessage.bind(this),
                    terminateVisibilityTimeout: true,
                    visibilityTimeout: 60,
                    waitTimeSeconds: 20,
                    batchSize: 1,
                    sqs: this.sqs,
                });
                this.logger.info('Subscriber is ready');
            }
        }
        catch (err) {
            this.logger.error(err, err.message);
            this.logger.warn('Subscriber will be disabled');
        }
    }

    async initialize(): Promise<any> {
        try {
            await this.sns.send(new ListTopicsCommand({}));
            await this.sqs.send(new ListQueuesCommand({}));
        }
        catch (e) {
            this.logger.error('Failed to connect AWS', e.message);
            throw e;
        }
    }

    async connect(): Promise<void> {
        if (!this.client) {
            return Promise.resolve();
        }
        return Promise.resolve()
            .then(() => this.client.start());
    }

    afterConnect(): Promise<void> {
        return Promise.resolve();
    }

    disconnect(): Promise<void> {
        if (!this.client) {
            return Promise.resolve();
        }
        return Promise.resolve()
            .then(() => this.client.stop());
    }

    public async handleMessage(message: any) {
        try {
            const body = JSON.parse(message.Body);
            const event = new Message(JSON.parse(body.Message));
            this.logger.info('Received message', event);

            const subscribers = this.subscribers.filter(sub => sub.messageType === event.type);
            if (subscribers.length) {
                for (const { handler, converter } of subscribers) {
                    const data = converter ? converter(event) : event;
                    await handler(data);
                }
            }
        }
        catch (err) {
            this.logger.error(err, err.message);
        }
    }

    public async publish(message: PublishMessage) {
        if (!this.options.snsTopic) {
            this.logger.info('[SNS] Publisher disabled')
            return;
        }
        const { eventType, data, options } = message;
        try {
            const event = new Message({
                source: this.options.source,
                type: eventType,
                data: data
            });

            const mergedOptions = this.mergeDefaultAttribute(eventType, options);

            const input = {
                TopicArn: this.options.snsTopic,
                Message: event.toString(),
                ...mergedOptions,
            } as PublishCommandInput;
            const command = new PublishCommand(input);

            await this.sns.send(command);

            this.logger.info('[SNS] Publish message successfully', { topic: this.options.snsTopic, data });
        }
        catch (error) {
            this.logger.error('[SNS] Failed to publish message', { topic: this.options.snsTopic, data, error });
            throw error;
        }
    }

    private mergeDefaultAttribute(eventType: string, options: Partial<PublishCommandInput> = {}) {
        return {
            ...options,
            MessageAttributes: {
                ...options.MessageAttributes,
                eventName: {
                    StringValue: eventType,
                    DataType: 'String'
                },
                environment: {
                    StringValue: process.env.ENVIRONMENT || 'default',
                    DataType: 'String'
                }
            }
        };
    }
}
