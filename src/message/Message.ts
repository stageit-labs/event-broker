import { CloudEvent } from "cloudevents";

export class Message<T> extends CloudEvent<T>{};