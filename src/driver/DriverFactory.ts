import { AwsDriver } from './aws/AwsDriver';
import { Driver } from './Driver';
import { Connection } from '../connection';
import { GoogleDriver } from './google/GoogleDriver';

export class DriverFactory {
    /**
     * Creates a new driver depend on a given connection's driver type.
     */
    create(connection: Connection): Driver {
        const { type } = connection.options;
        switch (type) {
        case 'aws':
            return new AwsDriver(connection);
        case 'google':
            return new GoogleDriver(connection);
        default:
            throw new Error('not support this driver!');
        }
    }
}
