import {registerEvent} from '../decorators/SubscriberRegister';
export class InstanceChecker {
    static isConnection(obj: unknown): boolean {
        return this.check(obj, 'Connection');
    }

    static isSubscriber(obj: unknown): boolean {
        return this.check(obj, 'Subscriber');
    }

    static isSubscriberHandler(obj: unknown): boolean {
        return this.check(obj, 'SubscriberHandler');
    }

    static isPublishMessage(obj: unknown): boolean {
        return this.check(obj, 'PublishMessage');
    }

    static isRegisteredEvent(obj: unknown): boolean {
        return (
            typeof obj === 'object' &&
            obj !== null &&
            obj[registerEvent] ===
                Symbol.for('registerEvent')
        );
    }

    private static check(obj: unknown, name: string) {
        return (
            typeof obj === 'object' &&
            obj !== null &&
            (obj as { '@instanceof': Symbol })['@instanceof'] ===
                Symbol.for(name)
        );
    }
}
