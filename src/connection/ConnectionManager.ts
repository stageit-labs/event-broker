import { Connection } from './Connection';
import { ConnectionOptions } from './ConnectionOptions';

class ConnectionManager {
    private readonly connectionMap: Map<string, Connection> = new Map()
    get connections(): Connection[] {
        return Array.from(this.connectionMap.values());
    }

    /**
     * Checks if connection with the given name exist in the manager.
     */
    has(name: string): boolean {
        return this.connectionMap.has(name);
    }

    /**
     * Gets registered connection with the given name.
     * If connection name is not given then it will get a default connection.
     * Throws error if connection with the given name was not found.
     */
    get(name: string = 'default'): Connection {
        const connection = this.connectionMap.get(name);
        if (!connection) throw new Error('Connection not found!');

        return connection;
    }

    /**
     * Creates a new connection based on the given connection options and registers it in the manager.
     * Connection won't be established, you'll need to manually call connect method to establish connection.
     */
    async create(options: ConnectionOptions): Promise<Connection> {
        // check if such connection is already registered
        const existConnection = this.connectionMap.get(
            options.name || 'default',
        );
        if (existConnection) {
            // if connection is registered and its not closed then throw an error
            if (existConnection.isInitialized)
                throw new Error('Already has active connection');
        }

        // create a new connection
        const connection = new Connection(options);
        await connection.initialize();
        this.connectionMap.set(connection.name, connection);
        return connection;
    }
}

export const connectionManager = new ConnectionManager();
