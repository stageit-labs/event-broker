import { PublishMessage } from "./PublishMessage";
enum JOB_EVENT_TYPE {
    ADD_JOB = "com.icondo.job.added",
    UPDATE_JOB = "com.icondo.job.updated",
    DELETE_JOB = "com.icondo.job.deleted",
}

type jobType = 'add' | 'update' | 'delete';

interface JobBase {
    type: jobType,
    queueName: string;
    jobType: string;
}


interface JobOptions {
    executeTime?: string | Date;
    delay?: number;
    jobId?: string;
    overrideExisting?: boolean;
}

interface AddJob extends JobBase {
    type: 'add';
    queueName: string;
    jobType: string;
    jobData: any;
    jobOptions: JobOptions;
}

interface UpdateJob extends JobBase {
    type: 'update',
    queueName: string;
    jobType: string;
    jobData: any;
    jobOptions: JobOptions;
}

interface DeleteJob extends JobBase {
    type: 'delete';
    queueName: string;
    jobType: string;
    jobOptions: {
        jobId: string
    }
}

type Job = JobBase | AddJob | UpdateJob | DeleteJob;

export function createJobMessageFactory(job: Job): PublishMessage {
    let eventType = null;
    const type = job.type;
    switch (type) {
        case 'add':
            eventType = JOB_EVENT_TYPE.ADD_JOB;
            break;
        case 'update':
            eventType = JOB_EVENT_TYPE.UPDATE_JOB;
            break;
        case 'delete':
            eventType = JOB_EVENT_TYPE.DELETE_JOB;
            break;
    }
    if (!eventType) {
        throw new Error('Job type invalid');
    }

    return new PublishMessage(eventType, job)
}