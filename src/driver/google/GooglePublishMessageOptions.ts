import {Attributes} from '@google-cloud/pubsub'
import { BaseMessageOptions } from '../../message';

export interface GooglePublishMessageOptions extends BaseMessageOptions, Attributes {
    readonly type: 'google';
}
