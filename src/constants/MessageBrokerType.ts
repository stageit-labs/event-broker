export type MessageBrokerType =
        | 'aws'
        | 'google'
        | 'kafka'
