import { AwsConnectionOptions } from '../driver/aws/AwsConnectionOptions';
import { GoogleConnectionOptions } from '../driver/google/GoogleConnectionOptions';

/**
 * ConnectionOptions is an interface with settings and options for specific Connection.
 */
export type ConnectionOptions =
        | AwsConnectionOptions
        | GoogleConnectionOptions;

