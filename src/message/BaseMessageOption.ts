import { MessageBrokerType } from '../constants/MessageBrokerType';

export interface BaseMessageOptions {
    readonly type: MessageBrokerType;
    readonly source: string;
}
