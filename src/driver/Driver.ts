import { Subscriber } from '../subscribers';
import {BaseConnectionOptions} from '../connection';
import { PublishMessage } from '../message';
export interface Driver {
    /**
     * Connection options.
     */
    options: BaseConnectionOptions;

    subscribers: Subscriber<any, any>[];

    initialize(): Promise<any>;

    connect(): Promise<void>;

    afterConnect(): Promise<void>;

    disconnect(): Promise<void>;

    publish(message: PublishMessage): Promise<void>;

}
