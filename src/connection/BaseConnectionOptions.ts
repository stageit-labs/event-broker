import { MessageBrokerType } from '../constants/MessageBrokerType';

export interface BaseConnectionOptions {
    readonly type: MessageBrokerType;
    readonly name: string;
    readonly logger: any;
    readonly source: string;
    readonly subscribers: string[];
    readonly sandbox: boolean;
}
