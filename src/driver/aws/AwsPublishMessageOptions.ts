import { PublishCommandInput } from '@aws-sdk/client-sns';
import { BaseMessageOptions } from '../../message';

export interface AwsPublishMessageOptions extends BaseMessageOptions, Partial<PublishCommandInput> {
    readonly type: 'aws';
}
