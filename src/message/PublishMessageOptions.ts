import { AwsPublishMessageOptions } from '../driver/aws/AwsPublishMessageOptions';
import { GooglePublishMessageOptions } from '../driver/google/GooglePublishMessageOptions';

export type PublishMessageOptions =
    | AwsPublishMessageOptions
    | GooglePublishMessageOptions
