import { PublishMessageOptions } from './PublishMessageOptions';

export class PublishMessage {
    readonly '@instanceof' = Symbol.for('PublishMessage');
    options: any;
    data: any;
    eventType: string;
    constructor(eventType: string, data: any, options?: PublishMessageOptions) {
        this.eventType = eventType;
        this.data = data;
        this.options = options;
    }
}