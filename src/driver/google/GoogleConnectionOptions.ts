import { BaseConnectionOptions } from '../../connection/BaseConnectionOptions';

export interface GoogleConnectionOptions extends BaseConnectionOptions {
    readonly type: 'google';
    readonly pathKeyFile?: string;
    readonly topicName?: string;
    readonly subscriberName?: string;
}
