import { Driver } from '../driver/Driver';
import { DriverFactory } from '../driver/DriverFactory';
import {importSubscriberFromDirectories} from '../util';
import {ConnectionOptions} from '.';
import { Subscriber } from '../subscribers';
import { PublishMessage } from '../message';

export class Connection {
    readonly '@instanceof' = Symbol.for('Connection')

    readonly name: string

    /**
     * Connection options.
     */
    readonly options: ConnectionOptions

    /**
     * Indicates if Connection is initialized or not.
     */
    readonly isInitialized: boolean

    /**
     * message broker driver used by this connection.
     */
    driver: Driver

    logger: any

    readonly subscribers: Subscriber<any, any>[] = []

    // -------------------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------------------

    constructor(options: ConnectionOptions) {
        this.name = options.name || 'default';
        this.options = options;
        this.logger = options.logger;
        this.driver = new DriverFactory().create(this);
        this.isInitialized = false;
    }

    // -------------------------------------------------------------------------
    // Public Methods
    // -------------------------------------------------------------------------
    /**
     * Updates current connection options with provided options.
     */
    setOptions(options: Partial<ConnectionOptions>): this {
        Object.assign(this.options, options);
        return this;
    }

    async initialize(): Promise<this> {
        if (this.isInitialized)
            throw new Error('Cannot Connect Already Connected Error');

        await this.loadSubscribers();
        await this.driver.initialize();
        await this.driver.connect();

        // set connected status for the current connection
        Object.assign(this, { isInitialized: true });

        try {
            await this.driver.afterConnect();
        }
        catch (error) {
            throw error;
        }

        return this;
    }

    async loadSubscribers() {
        this.driver.subscribers = await importSubscriberFromDirectories(this.options.subscribers);
    }

    /**
     * Closes connection with the message broker.
     */
    async destroy(): Promise<void> {
        if (!this.isInitialized)
            throw new Error('Cannot Execute Not Connected Error');

        await this.driver.disconnect();

        Object.assign(this, { isInitialized: false });
    }

    public async publishMessage(message: PublishMessage) {
        await this.driver.publish(message);
    }
}
