import * as path from 'path';
import * as fs from 'fs';
import * as dotenv from 'dotenv';
const WINDOWS_PATH_REGEXP = /^([a-zA-Z]:.*)$/;
const UNC_WINDOWS_PATH_REGEXP = /^\\\\(\.\\)?(.*)$/;

export function toPortablePath(filepath: string): string {
    if (process.platform !== `win32`) return filepath;

    if (filepath.match(WINDOWS_PATH_REGEXP))
        filepath = filepath.replace(WINDOWS_PATH_REGEXP, `/$1`);
    else if (filepath.match(UNC_WINDOWS_PATH_REGEXP))
        filepath = filepath.replace(
            UNC_WINDOWS_PATH_REGEXP,
            (match, p1, p2) => `/unc/${p1 ? `.dot/` : ``}${p2}`,
        );

    return filepath.replace(/\\/g, `/`);
}

/**
 * Cross platform isAbsolute
 */
export function isAbsolute(filepath: string): boolean {
    return !!filepath.match(/^(?:[a-z]:|[\\]|[\/])/i);
}

/**
     * Normalizes given path. Does "path.normalize".
     */
export function pathNormalize(pathStr: string): string {
    return path.normalize(pathStr);
}

/**
 * Gets file extension. Does "path.extname".
 */
export function pathExtName(pathStr: string): string {
    return path.extname(pathStr);
}

/**
 * Resolved given path. Does "path.resolve".
 */
export function pathResolve(pathStr: string): string {
    return path.resolve(pathStr);
}

/**
 * Synchronously checks if file exist. Does "fs.existsSync".
 */
export function fileExist(pathStr: string): boolean {
    return fs.existsSync(pathStr);
}

export function readFileSync(filename: string): Buffer {
    return fs.readFileSync(filename);
}

export function appendFileSync(filename: string, data: any): void {
    fs.appendFileSync(filename, data);
}

export async function writeFile(path: string, data: any): Promise<void> {
    return new Promise<void>((ok, fail) => {
        fs.writeFile(path, data, (err) => {
            if (err) fail(err);
            ok();
        });
    });
}

/**
 * Loads a dotenv file into the environment variables.
 *
 * @param path The file to load as a dotenv configuration
 */
export function dotenvConfig(pathStr: string): void {
    dotenv.config({ path: pathStr });
}
