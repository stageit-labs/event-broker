import * as glob from 'glob';
import * as _ from 'lodash';
import {pathNormalize, pathExtName, pathResolve} from './PathUtils';
import {InstanceChecker} from './InstanceChecker';
import {importOrRequireFile} from './ImportUtils';
import { Subscriber } from '../subscribers';

/**
 * Loads all exported classes from the given directory.
 */
export async function importSubscriberFromDirectories(
    directories: string[],
    formats = ['.js', '.ts'],
): Promise<Subscriber<any, any>[]> {
    function loadFileClasses(exported: any, allLoaded: Subscriber<any, any>[]) {
        if (
            InstanceChecker.isSubscriber(exported) && InstanceChecker.isRegisteredEvent(exported)
        ) {
            allLoaded.push(exported);
        }
        else if (Array.isArray(exported)) {
            exported.forEach((i: any) => loadFileClasses(i, allLoaded));
        }
        else if (_.isObject(exported)) {
            Object.keys(exported).forEach((key) =>
                loadFileClasses(exported[key], allLoaded),
            );
        }
        return allLoaded;
    }

    const allFiles = directories.reduce((allDirs, dir) => {
        return allDirs.concat(glob.sync(pathNormalize(dir)));
    }, [] as string[]);

    const dirPromises = allFiles
        .filter((file) => {
            const dtsExtension = file.substring(file.length - 5, file.length);
            return (
                formats.indexOf(pathExtName(file)) !== -1 &&
                dtsExtension !== '.d.ts'
            );
        })
        .map(async (file) => {
            const [importOrRequireResult] = await importOrRequireFile(
                pathResolve(file),
            );
            return importOrRequireResult;
        });

    const dirs = await Promise.all(dirPromises);

    return loadFileClasses(dirs, ([] as Subscriber<any, any>[]));
}

