import {ConnectionManager} from '../connection';
import { PublishMessage } from '../message';
import { InstanceChecker } from '../util/InstanceChecker';

export function PublishRegister(connectionName: string): MethodDecorator {
    return function(
        target: Object,
        key: string | symbol,
        descriptor: PropertyDescriptor
    ) {
        const getMessage = descriptor.value;

        descriptor.value = (...args: any[]) => {
            const connection = ConnectionManager.get(connectionName);
            const message = getMessage.apply(target, args) as PublishMessage;
            if (!InstanceChecker.isPublishMessage(message)) {
                throw Error('Can not publish this message');
            }
            connection.publishMessage(message);
        };

        return descriptor;
    };
}
