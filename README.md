# Event Broker

#Step by Step Guide

1 Create connection.
- Use ConnectionManager create connection with specific type (aws, google)
```
import * as path from 'path';
import {ConnectionManager, Message, PublishRegister} from '@stageit-labs/event-broker';
import { IOCServiceName } from 'ioc/IocServiceName';
import Container, { Service } from 'typedi';
import ILogger from '@stageit-labs/aws-client/dist/base/ILogger';

await ConnectionManager.create({
    type: 'aws',
    snsTopic: 'arn:aws:sns:ap-southeast-1:950460482435:icondo-review-video-call-vqoug1-events',
    queueUrl: 'https://sqs.ap-southeast-1.amazonaws.com/950460482435/icondo-review-dac-vttj0h-dac-events',
    batchSize: 1,
    name: 'test',
    sandbox: true,
    logger: logger,
    source: 'test',
    subscribers: [path.join(__dirname, '../test_event/subscribers/*{.js,.ts}')],
    visibilityTimeout: 10,
    terminateVisibilityTimeout: false,
    accessKeyId: 'AKIA52S64AOBU47EMQ6S',
    region: 'ap-southeast-1',
    secretAccessKey: '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',
    waitTimeSeconds: 10
});
```
- Or with google type

```
const url = path.join(__dirname, "../../", "configs/env", "firebase.json");
await ConnectionManager.create({
    type: "google",
    pathKeyFile: url,
    logger,
    source: "test",
    name: "google_test",
    sandbox: false,
    subscribers: [],
    topicName: "test",
    subscriberName: "test",
});
```
2 Create Subscriber
- Use SubscriberRegister and extends from Subscriber

```
import { CLOUD_EVENT_TYPE } from 'app/constants/CloudEvents';
import { SubscriberRegister, Subscriber} from '@stageit-labs/event-broker';
@SubscriberRegister('bookingCreate')
class BookingSubscriber extends Subscriber {
    public handler(message: any): Promise<any> {
        console.log(message);
        return Promise.resolve();
    }
}

export default new BookingSubscriber();
```
3 Create Publisher
- Use decorator PublishRegister and method return type Message

```
@Service('BookingPublisher')
class BookingPublisher {
    @PublishRegister('google_test')
    publishCreateBooking() {
        return new Message('bookingCreate', {test: 'hello'});
    }
}