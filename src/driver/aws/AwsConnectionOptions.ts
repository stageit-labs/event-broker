import { BaseConnectionOptions } from '../../connection/BaseConnectionOptions';

export interface AwsConnectionOptions extends BaseConnectionOptions {
    readonly type: 'aws';
    readonly accessKeyId?: string;
    readonly secretAccessKey?: string;
    readonly region: string;
    readonly queueUrl?: string;
    readonly snsTopic?: string;
    readonly terminateVisibilityTimeout: boolean;
    readonly visibilityTimeout: number;
    readonly waitTimeSeconds: number;
    readonly batchSize: number;
}
