import { PublishCommandInput } from "@aws-sdk/client-sns";

export type AwsPublishOption = Partial<PublishCommandInput>;
