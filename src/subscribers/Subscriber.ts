import { Message } from '../message';
export type Converter<T, Model> = (data: Message<T>) => Model;
export abstract class Subscriber<T, Model> {
    public converter?: Converter<T, Model>;
    readonly '@instanceof' = Symbol.for('Subscriber');
    public messageType: string;
    public abstract handler(message: Model): Promise<any>;
}
