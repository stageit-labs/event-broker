import { PubSub, Topic, Subscription, Message as GoogleMessage } from '@google-cloud/pubsub';
import {Message}  from '../../message';
import { Driver } from '../Driver';
import { Connection } from '../../connection';
import { GoogleConnectionOptions } from './GoogleConnectionOptions';
import { Subscriber } from '../../subscribers';
import { PublishMessage } from '../../message';

export class GoogleDriver implements Driver {
    public subscribers: Subscriber<any, any>[];
    public options: GoogleConnectionOptions;
    private logger: any;
    private client: PubSub;
    private topic: Topic;
    private subscriber: Subscription;


    constructor(connection: Connection) {
        this.logger = connection.logger;
        this.options = connection.options as GoogleConnectionOptions;
        this.subscribers = connection.subscribers;

        try {
            this.client = new PubSub({ keyFilename: this.options.pathKeyFile });
            if (this.options.subscriberName) {
                this.subscriber = this.client.subscription(this.options.subscriberName);
            }
            if (this.options.topicName) {
                this.topic = this.client.topic(this.options.topicName);
            }
            // Receive callbacks for new messages on the subscription
            this.subscriber.on('message', message => {
                this.handleMessage(message);
            });

            // Receive callbacks for errors on the subscription
            this.subscriber.on('error', error => {
                console.error('Received error:', error);
            });
            this.logger.info('[Google Pub Sub] Subscriber is ready');
        }
        catch (err) {
            this.logger.error(err, err.message);
            this.logger.warn('[Google Pub Sub] Subscriber will be disabled');
        }
    }

    async initialize(): Promise<any> {
        try {
            await this.client.getTopics()
            await this.client.getSubscriptions();
        }
        catch (e) {
            this.logger.error('Failed to connect Google Pub Sub', e.message);
            throw e;
        }
    }

    async connect(): Promise<void> {
        this.subscribers.forEach(sub => {
            this.subscriber.on(sub.messageType, sub.handler);
        });
    }

    afterConnect(): Promise<void> {
        return Promise.resolve();
    }

    disconnect(): Promise<void> {
        return Promise.resolve();
    }

    public async handleMessage(message: GoogleMessage) {
        let isParseDataError = true;
        try {
            const body = JSON.parse(message.data.toString());
            const event = new Message(body);
            isParseDataError = false;
            this.logger.info(body, 'Received message');

            const subscribers = this.subscribers.filter(sub => sub.messageType === event.type);
            if (subscribers.length) {
                for (const { handler, converter } of subscribers) {
                    const data = converter ? converter(event) : event;
                    await handler(data);
                }
            }
            message.ack();
        }
        catch (err) {
            this.logger.error(err, err.message);
            if (isParseDataError) {
                message.ack();
            }
        }
    }

    public async publish(message: PublishMessage) {
        if (!this.options.topicName) {
            this.logger.info('[Google Pub Sub] Publisher disabled')
            return;
        }
        const { data, eventType, options } = message;
        try {
            const event = new Message({
                source: this.options.source,
                type: eventType,
                data: data
            });

            const bufferData = Buffer.from(event.toString());
            const mergedAttributes = this.mergeDefaultAttribute(eventType, options);
            await this.topic.publishMessage({ data: bufferData, attributes: mergedAttributes })

            this.logger.info('[Google Pub Sub] Publish message successfully', { topic: this.options.topicName, data });
        }
        catch (error) {
            this.logger.error('[Google Pub Sub] Failed to publish message', { topic: this.options.topicName, data, error });
            throw error;
        }
    }

    private mergeDefaultAttribute(eventType: string, options: any = {}) {
        return {
            ...options,
            eventName: eventType,
            environment: process.env.ENVIRONMENT || 'default'
        };
    }
}
