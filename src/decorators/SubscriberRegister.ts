import { Converter } from "../subscribers";

export const registerEvent = Symbol('registerEvent');
export function SubscriberRegister<Message, Model>(messageType: string, converter?: Converter<Message, Model>): ClassDecorator {
    return function(constructor: Function) {
        constructor.prototype[registerEvent] = Symbol.for('registerEvent');
        constructor.prototype.messageType = messageType;
        constructor.prototype.converter = converter;
    };
}
